//
//  MyProtocol.h
//  Learning0509 delegate
//
//  Created by  apple on 13-5-9.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyProtocol <NSObject>

-(void)doSomething: (NSString *)thing;
-(void)dodo;

@end
