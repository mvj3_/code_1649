//
//  B.m
//  Learning0509 delegate
//
//  实现MyProtocol中的方法
//
//  Created by  apple on 13-5-9.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "B.h"

@implementation B

-(void)doSomething: (NSString * ) thing{
    NSLog(@"%@", thing);
}

-(void)dodo{
    NSLog(@"dodo");
}

@end
