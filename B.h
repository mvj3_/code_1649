//
//  B.h
//  Learning0509 delegate
//
//  B类采用MyProtocol协议
//
//  Created by  apple on 13-5-9.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyProtocol.h"

@interface B : NSObject <MyProtocol>

@end
