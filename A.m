//
//  A.m
//  Learning0509 delegate
//
//  Created by  apple on 13-5-9.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "A.h"
#import "B.h"

@implementation A
@synthesize delegate;

- (id)init
{
    self = [super init];
    if (self) {
        //delegate = B;
    }
    return self;
}

-(void)doSomethingByDelegate:(NSString *)thing{
    [delegate doSomething:thing];
}

-(void)dodoByDelegate{
    [delegate dodo];
}

@end
