//
//  main.m
//  Learning0509 delegate
//
//  Created by  apple on 13-5-9.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "A.h"
#import "B.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        A * a = [[A alloc] init];
        B * b = [[B alloc] init];
        [a setDelegate:b];
        [a doSomethingByDelegate:@"hello"];
        [a dodoByDelegate];

        [a release];
        [b release];
    };
    return 0;
}

