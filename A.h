//
//  A.h
//  Learning0509 delegate
//
//  Created by  apple on 13-5-9.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyProtocol.h"

@interface A : NSObject
{
    id<MyProtocol> delegate; //声明一个委托对象，要把事情委托给实现了MyProtocol的类
}
@property (assign, nonatomic) id<MyProtocol> delegate;

-(void)doSomethingByDelegate:(NSString *)thing;
-(void)dodoByDelegate;

@end
